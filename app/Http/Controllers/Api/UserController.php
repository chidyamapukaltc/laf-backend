<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public $successStatus = 200;

    /**
     * Registration
     */
    public function register(Request $request)
    {

        $data = $request->json()->all();
        $validation = Validator::make($data, [
            'firstname' => ['required', 'string','min:3',  'max:255'],
            'lastname' => ['required', 'string','min:3', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($validation->passes()) {
            $user = User::create([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'role' => 'User',
                'password' => bcrypt($data['password'])
            ]);

            $token = $user->createToken('LaravelAuthApp')->accessToken;

            return response()->json([
                'success' => true,
                'token' => $token,
                'data' => $user], 200);
        }else{
            return response()->json([
                'success' => false,
                'error' =>$validation->errors()->first()
            ], 400);
        }

    }

    /**
     * Login
     */
    public function login(Request $request)
    {
        $data = $request->json()->all();
        $validation = Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            return response()->json([
                'success' => true,
                'token' => $token,
                'data' => auth()->user()], 200);
        } else {
            if ($validation->errors()->first()){
                return response()->json([
                    'success' => false,
                    'error' => $validation->errors()->first()], 401);
            }else{
                return response()->json([
                    'success' => false,
                    'error' => "Your useremail and password combination is wrong"], 401);
            }

        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
